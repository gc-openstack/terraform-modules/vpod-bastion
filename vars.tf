variable "vpod_name" {
  type = string
}

variable "ctrl_network_id" {
  type = string
}

variable "ctrl_subnet_id" {
  type = string
}

variable "storage_network_id" {
  type = string
}

variable "storage_subnet_id" {
  type = string
}

variable "cidr_hint" {
  type = number
}

variable "vpod_floatingip" {
}

variable "vpod_common_sg_id" {
  type = string
}

#
# Optional
#
variable "dns_base" {
  type    = string
  default = "openstack.example.net."
}

variable "flavor" {
  type    = string
  default = "tiny"
}

variable "image" {
  type    = string
  default = "ubuntu-18.04-ofed-5.4"
}

variable "key_pair" {
  type    = string
  default = "seed"
}

variable "bastionaz" {
  type = string
  default = "general"
}
